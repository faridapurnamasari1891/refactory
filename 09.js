const arrayList = [3, 12, 4, 5, 8, 9]

const sortMethod = (items) => {
    for (var i = 0; i < items.length; i++) { 
        for (var j = 0; j < (items.length - i - 1); j++) { 
            if(items[j] > items[j+1]) {
                var tmp = items[j];
                items[j] = items[j+1];
                items[j+1] = tmp;
            }
        }        
    }
    return console.log(items);
}

sortMethod(arrayList);