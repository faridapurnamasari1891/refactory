const numbers = [9,4,2,4,1,5,3,0];

function sort_odd (items) {
  for (var i = 0; i < items.length - 1; i++) {
    if (items[i] % 2 === 1) {
      for (var j = i + 1; j < items.length; j++) {
        if (items[j] % 2 === 1) {
          if (items[i] > items[j]) {
            var tmp = items[j];
            items[j] = items[i];
            items[i] = tmp;
          }
        }
      }
    }
  }
  return console.log(items);
}

sort_odd(numbers);