const list_letters_first = ["c","d","e","g","h"];
const list_letters_second = ["X","Z"]

function missingLetter(str) {
    var allChars = "";
    var notChars = new RegExp("[^" + str + "]", "g");

    for (var i = 0; allChars[allChars.length - 1] !== str[str.length - 1]; i++)
        allChars += String.fromCharCode(str[0].charCodeAt(0) + i);

    return allChars.match(notChars)
        ? allChars.match(notChars).join("")
        : undefined;
}

console.log(missingLetter(list_letters_first));
console.log(missingLetter(list_letters_second));