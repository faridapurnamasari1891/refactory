const censor = ["dolor", "elit", "quis", "nisi", "fugiat", "proident", "laborum"];
const paragraph = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\nUt enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";
const char = '*';
const censoring = (sentence) => {
    const replaceWord = (match) => char.repeat(match.length);
    const filter = new RegExp(`\\b(${censor.join('|')})\\b`, 'gi');
    return sentence.replace(filter, replaceWord);
  }
  
console.log(censoring(paragraph));

/*
nb:
"dolor" di kalimat ke-3 pada contoh output dalam soal tidak di sensor
soalnya salah ketik atau tidak?
*/